/* ng-infinite-scroll - v1.0.0 - 2013-02-23 */

(function()
{
	var mod;

	mod = angular.module('infinite-scroll', []);

	mod.directive('infiniteScroll', [
	  '$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
		return {
		  link: function(scope, elem, attrs) {
			var scrollNode = angular.element(document).find(attrs.infiniteScrollNodeSelector),
				scrollDistance = 0,
				checkWhenEnabled, scrollDistance, 
				scrollEnabled, isScrollEnd, 
				onNodeScrolled, checkScrollEnd;

			if (attrs.infiniteScrollDistance != null) {
			  scope.$watch(attrs.infiniteScrollDistance, function(value) {
				return scrollDistance = parseFloat(value);
			  });
			}
			scrollEnabled = true;
			checkWhenEnabled = false;
			if (attrs.infiniteScrollDisabled != null) {
			  scope.$watch(attrs.infiniteScrollDisabled, function(value) {
				scrollEnabled = !value;
				checkScrollEnd();
				if (scrollEnabled && checkWhenEnabled) {
				  checkWhenEnabled = false;
				  return onNodeScrolled();
				}
			  });
			};

			scope.$watch(attrs.infiniteScrollEnd, function(value) {
				isScrollEnd = value;
				checkScrollEnd();
			});

			checkScrollEnd = function()
			{
				//если скролл доскролирован до конца, но признак окончания загрузки данных говорит, что не все данные загружены,
				//нужно ещё раз вызывать функцию получения данных
				if (isScrollEnd || !scrollEnabled || scrollNode[0].scrollTop !== scrollNode[0].scrollHeight) 
					return;

				if ($rootScope.$$phase) 
					return scope.$eval(attrs.infiniteScroll);
				else 
					return scope.$apply(attrs.infiniteScroll);
			}

			onNodeScrolled = function(event, data)
			{
				var elementBottom, remaining, handler,
					shouldScroll, windowBottom;

				windowBottom = scrollNode.height() + scrollNode.scrollTop();
				elementBottom = /*elem.offset().top + */elem.height();
				remaining = elementBottom - windowBottom;
				shouldScroll = remaining <= scrollNode.height() * scrollDistance;
				if (shouldScroll && scrollEnabled) {
					if ($rootScope.$$phase) {
					  return scope.$eval(attrs.infiniteScroll);
					} else {
					  return scope.$apply(attrs.infiniteScroll);
					}
				} else if (shouldScroll) {
					return checkWhenEnabled = true;
				}
				// return $timeout((function() {
				//   if (attrs.infiniteScrollImmediateCheck) {
				// 	if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
				// 	  return handler();
				// 	}
				//   } else {
				// 	return handler();
				//   }
				// }), 0);
			}

			scrollNode.on('scroll', onNodeScrolled)
			scope.$on('$destroy', function() {
				return scrollNode.off('scroll', onNodeScrolled);
			});
		  }
		};
	  }
	]);
})();