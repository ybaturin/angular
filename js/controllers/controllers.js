'use strict';

/* Controllers */

(function() {
	var hdserials = angular.module('hdserials.controllers', []), 
		FIRST_GET_VIDEOS_LIMIT = 100,
		JSONP_URL = "http://hdserials.galanov.net/backend/model.php?skip_ua=yep&callback=JSON_CALLBACK&",
		GENERAL_CATEGORIES_ID = "common-categories";

	hdserials.controller('PageController', ['$scope', function($scope) {
		$scope.pageInfo = {
			title: ''
		};
	}]);

	hdserials.controller('CategoriesController', ['$scope', '$stateParams', 'VideosService', 'WaiterService', function($scope, $stateParams, videosService, waiterService) {
		//восстанавливаем состояние выделенной категории, т.к. после отработки
		//ng-switch контроллер переинициализируется
		$scope.selectedCategorie = videosService.getSelectedCategorie();

		$scope.toggleCategorie = function(targetCategorie) {	
			var catId = targetCategorie.id;		

			if (targetCategorie === $scope.selectedCategorie)
			{
				$scope.selectedCategorie = null;
				videosService.setSelectedCategorie(null);
				$scope.$broadcast('subCategoriesVisibleChanged', { isVisible: false, catId: catId });
				return;
			}

			$scope.selectedCategorie = targetCategorie;
			videosService.setSelectedCategorie(targetCategorie);
			
			waiterService.show();
			$scope.subCategories = null;

			videosService.getSubCategories(catId).then(function(subCategories){
				$scope.$broadcast('subCategoriesVisibleChanged', { isVisible: true, catId: catId });
				$scope.subCategories = subCategories;
				waiterService.hide();
			});
		};
		
		videosService.getGeneralCategories().then(function(generalCategories){
			var categorie, i;
			$scope.generalCategories = generalCategories;
			for (i = 0; i < generalCategories.data.length; i++)
			{
				categorie = generalCategories.data[i];
				categorie.icon = $scope.icons[i % $scope.icons.length];
			}
		});
	}]);

	hdserials.controller('VideosListController', ['$scope', '$rootScope', '$state', '$stateParams', 'VideosService', 'WaiterService', 'StorageService', 
		function($scope, $rootScope, $state, $stateParams, videosService, waiterService, storageService) {
		var categorieId = $stateParams.id || 0,
			firstVideosLoading = true,
			loadedCount = -1;

		$scope.partVideosLoading = false;
		$scope.allVideosLoaded = false;
		$scope.videos = [];
	
		//$scope.$parent.pageInfo.title = 'Категория №' + categorieId; // подставить имя категории после загрузки данных
		$scope.$parent.pageInfo.title = '';

		if (!angular.isDefined(categorieId)) 
			return;

		$scope.getNextPartVideos = function(limit) {
			if ($scope.allVideosLoaded)
				return;

			if (firstVideosLoading)
				waiterService.show();

			$scope.sortField = storageService.getSortField();
			$scope.isSortByRating = $scope.sortField.indexOf('rating') !== -1;

			$scope.partVideosLoading = true;
			videosService.getVideosList(categorieId, loadedCount + 1, limit).then(function(videosList){
				$scope.videos = $scope.videos.concat(videosList.data);
				$scope.partVideosLoading = false;

				loadedCount = $scope.videos.length;
				$scope.allVideosLoaded = !!videosList.endOfData;

				if (firstVideosLoading)
				{
					waiterService.hide();
					firstVideosLoading = false;
				}
			});
		};

		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};

		$rootScope.$on('sortSettingsChanged', function() {
			firstVideosLoading = true;
			$scope.allVideosLoaded = false;
			loadedCount = -1
			$scope.videos = [];
			$scope.getNextPartVideos(FIRST_GET_VIDEOS_LIMIT);
		});

		$scope.getNextPartVideos(FIRST_GET_VIDEOS_LIMIT);
	}]);
		
	hdserials.controller('VideoDetailsController', ['$scope', '$state', '$stateParams', 'VideosService', 'WaiterService', 'StorageService', 'videoDetails', 
			function($scope, $state, $stateParams, videosService, waiterService, storageService, videoDetails) {

		var i;
				
		// collapse all seasons
		if (videoDetails.hasSeasons) {
			for (i in videoDetails.seasons) {
				videoDetails.seasons[i].collapsed = true;
			}
		}

		$scope.video = videoDetails;
		$scope.$parent.pageInfo.title = videoDetails.info.title_ru;
		
		$scope.playVideoFile = function(fileId)	 {
			$state.go('video.play', { fileId: fileId });	
		}

		$scope.toggleViewedState = function(event, episode) {
			episode.viewed = !episode.viewed;
			storageService.toggleViewedFile(episode.id);
			event.stopPropagation();
			event.preventDefault();
		}

		$scope.toggleSeasonCollapsed = function(key) {
			/*var seasons, i;
			seasons = $scope.video.seasons;

			// collapse other seasons
			for (i in seasons) {
				if (i === key) {
					// toggle selected season
					seasons[i].collapsed = !seasons[i].collapsed;
					continue;
				}
				seasons[i].collapsed = true;
			}*/
		}
	}]);

	hdserials.controller('VideoGridItemController', ['$scope', '$state', function($scope, $state) {
		$scope.isRatingShow = false;
		if ($scope.$parent.isSortByRating)
		{
			$scope.rating = $scope.video[$scope.$parent.sortField];
			$scope.isRatingShow = true;
		}

		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};
	}]);

	hdserials.controller('NewsGridItemController', ['$scope', '$state', function($scope, $state) {
		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};
	}]);

	hdserials.controller('VideoPlayerController', ['$scope', '$state', '$stateParams', 'VideosService', 'videoId', 'videoDetails', 'StorageService', 'BrowserDetector',
		function($scope, $state, $stateParams, videosService, videoId, videoDetails, storageService, browserDetector) {
		var fileId = $stateParams.fileId,
			currentTime = $stateParams.currentTime,
			fileTitle, videoFiles, fileInfo, i;

		if (!fileId)
			return;
		
		currentTime = Math.max(0, currentTime - 5);
		videoFiles = $scope.video.files;
		for (i = 0; i < videoFiles.length; i++)
		{
			if (videoFiles[i].id !== fileId)
				continue;

			fileInfo = videoFiles[i];
			fileTitle = videoDetails.info.title_ru;
			if (fileInfo.season != '0') {
				fileTitle += ', Сезон ' + fileInfo.season + ', ' + fileInfo.title;
			}
			break;
		}

		videosService.getVideoFileInfo(fileId).then(function(fileInfo) {
			var playMode;
			var src = 'http://media.w3.org/2010/05/sintel/trailer.mp4';
			if (fileInfo.type === 'hls') {
				//playMode = (browserDetector.isMobile() || browserDetector.isSmartTV()) ? 'html5' : 'flash';
				 playMode = 'html5';
				$scope.file = {
					title: fileTitle,
					src: 'video/sample_10mb.mp4',
					//src: playMode === 'html5' ? fileInfo.sources.m3u8.auto : fileInfo.sources.f4m,
					playMode: playMode,
					currentTime: currentTime
				};
			}
			else {
				// VK video file
				$scope.file = {
					title: fileTitle,
					// src: src,
					src: fileInfo.url,
					playMode: 'iframe',
					currentTime: currentTime
				};
			}
		});

		$scope.closePlayer = function() {
			$scope.saveVideoTiming();
			$state.go('^');
		}

		$scope.saveVideoTiming = function() {
			$scope.$broadcast('requestVideoTiming');
		}

		$scope.$on('responseVideoTiming', function(event, args) {
			storageService.updateLastViewedInfo($scope.video.info.id, fileId, args);
		});
	}]);

	// hdserials.controller('VideoPlayerController', ['$scope', '$state', '$stateParams', 'VideosService', 'videoId', 'videoDetails', 'StorageService', 'BrowserDetector',
	// 	function($scope, $state, $stateParams, videosService, videoId, videoDetails, storageService, browserDetector) {
	// 	var fileId = $stateParams.fileId,
	// 		currentTime = $stateParams.currentTime || 0,
	// 		fileTitle, videoFiles, fileInfo, i;

	// 	if (!fileId)
	// 		return;
		
	// 	currentTime = Math.max(0, currentTime - 5);
	// 	videoFiles = $scope.video.files;
	// 	for (i = 0; i < videoFiles.length; i++)
	// 	{
	// 		if (videoFiles[i].id !== fileId)
	// 			continue;

	// 		fileInfo = videoFiles[i];
	// 		fileTitle = videoDetails.info.title_ru;
	// 		if (fileInfo.season != '0') {
	// 			fileTitle += ', Сезон ' + fileInfo.season + ', ' + fileInfo.title;
	// 		}
	// 		break;
	// 	}

	// 	videosService.getVideoFileInfo(fileId).then(function(fileInfo) {
	// 		var playMode;
	// 		var src = 'http://media.w3.org/2010/05/sintel/trailer.mp4';
	// 		if (fileInfo.type === 'hls') {
	// 			playMode = 'html5';
	// 			$scope.file = {
	// 				title: fileTitle,
	// 				src: src,
	// 				playMode: playMode,
	// 				currentTime: currentTime
	// 			};
	// 		}
	// 		else {
	// 			// VK video file
	// 			$scope.file = {
	// 				title: fileTitle,
	// 				src: src,
	// 				playMode: 'iframe',
	// 				currentTime: currentTime
	// 			};
	// 		}
	// 	});

	// 	$scope.closePlayer = function() {
	// 		$scope.saveVideoTiming();
	// 		$state.go('^');
	// 	}

	// 	$scope.saveVideoTiming = function() {
	// 		$scope.$broadcast('requestVideoTiming');
	// 	}

	// 	$scope.$on('responseVideoTiming', function(event, args) {
	// 		storageService.updateLastViewedInfo($scope.video.info.id, fileId, args);
	// 	});
	// }]);

	hdserials.controller('SearchBoxController', ['$scope', '$rootScope', '$state', '$stateParams', function($scope, $rootScope, $state, $stateParams) {
		$scope.searchString = '';

		$scope.search = function() {
			if ($scope.searchString.length < 3)
				return;
			$state.go('search', { name: $scope.searchString });
		};
		//подписываемся на изменение state, чтобы при переходе по прямой ссылке поиска 
		//в текстовом блоке отобразить запрос
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
			if (toState.name === 'search')
				$scope.searchString = toParams.name;
		});
	}]);

	hdserials.controller('SearchStateController', ['$scope', '$state', '$stateParams', 'VideosService', 'WaiterService', function($scope, $state, $stateParams, videosService, waiterService) {
		var name = $stateParams.name,
			searchArgs;

		$scope.videos = null;
		$scope.notFoundMsg = null;
		$scope.$parent.pageInfo.title = 'Поиск'; // TODO вынести в ресурсы

		if (!angular.isDefined(name)) 
			return;

		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};

		searchArgs = {
			name: name
		};

		waiterService.show();
		videosService.searchVideos(searchArgs).then(function(videos){
			var isEmptyVideos = videos.length === 0 || (videos.length === 1 && angular.isString(videos[0]) && videos[0].indexOf('failed') !== -1)
			if (isEmptyVideos)
				$scope.notFoundMsg = 'Фильмов с названием, содержащим "' + name + '" не найдено'
			else
				$scope.videos = videos;

			waiterService.hide();
		});
	}]);

	hdserials.controller('NewsStateController', ['$scope', '$state', '$stateParams', 'VideosService', 'WaiterService', function($scope, $state, $stateParams, videosService, waiterService) {
		$scope.videos = null;

		$scope.$parent.pageInfo.title = 'Обновления сериалов'; // TODO вынести в ресурсы

		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};

		waiterService.show();
		videosService.getNews().then(function(videos){
			$scope.videos = videos;
			waiterService.hide();
		});
	}]);

	hdserials.controller('FavoriteBoxController', ['$scope', '$state', '$stateParams', 'StorageService', function($scope, $state, $stateParams, storageService) {
		$scope.toggleFavoriteVideo = function(video)
		{
			if (!video)
				return;

			storageService.toggleFavoriteVideo(video);
		}
		$scope.isFavoriteVideo = function(video)
		{
			return video ? storageService.isFavoriteVideo(video) : false;
		}
	}]);

	hdserials.controller('FavoritesStateController', ['$scope', '$state', '$stateParams', 'StorageService', function($scope, $state, $stateParams, storageService) {
		var videos = storageService.getFavoriteVideos();

		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};

		$scope.videos = storageService.getFavoriteVideos();
		$scope.emptyFavorites = videos.length === 0;

		$scope.$parent.pageInfo.title = 'Избранное'; // TODO вынести в ресурсы
	}]);

	hdserials.controller('NavigatorController', ['$scope', '$state', '$stateParams', 'StorageService', function($scope, $state, $stateParams, storageService) {
		$scope.toggleMenuItem = function (selectedItem){
			var item, i;

			if (selectedItem.selected)
			{
				//кликнули на уже выбранный пункт меню, нужно скрыть меню
				selectedItem.selected = false;
				$scope.selectedItem = null
			}
			else
			{
				for (i = 0; i < $scope.items.length; i++)
				{
					item = $scope.items[i];
					item.selected = item === selectedItem;
				}
				$scope.selectedItem = selectedItem;
				$scope.selectedMenuItemId = selectedItem.subMenuId;
			}
		}
	}]);

	hdserials.controller('FilterController', ['$scope', '$state', '$stateParams', 'VideosService', function($scope, $state, $stateParams, videosService) {
		//$scope.$parent.pageInfo.title = 'Расширенный поиск'; // TODO вынести в ресурсы

		$scope.filterOpt = {
			country: null,
			genre: null,
			year_from: null,
			year_to: null
		};		

		videosService.getFilterData().then(function(data){
			$scope.filterData = data;

			var years = [],
				startYear = parseInt(data.min_year, 10),
				endYear = parseInt(data.max_year, 10),
				year;

			for (year = endYear; year >= startYear; year--)
				years.push(year);

			$scope.filterData.years = years;
		})
	
		$scope.$watch('filterOpt.year_from', function(newVal, oldVal)
		{
			if (!newVal || !$scope.filterOpt.year_to)
				return;

			if (newVal > $scope.filterOpt.year_to)
				$scope.filterOpt.year_to = newVal;
		});


		$scope.$watch('filterOpt.year_to', function(newVal, oldVal)
		{
			if (!newVal || !$scope.filterOpt.year_from)
				return;

			if (newVal < $scope.filterOpt.year_from)
				$scope.filterOpt.year_from = newVal;
		});

		$scope.filter = function()
		{
			$state.go('filter', $scope.filterOpt);
		}
	}]);

	hdserials.controller('FilterStateController', ['$scope', '$state', '$stateParams', 'VideosService', 'WaiterService', function($scope, $state, $stateParams, videosService, waiterService) {
		$scope.showDetails = function(id) {
			$state.go('video', { id: id });
		};

		$scope.notFoundMsg = null;

		waiterService.show();
		videosService.filterVideos($stateParams).then(function(videos){
			var isEmptyVideos = videos.length === 0 || (videos.length === 1 && angular.isString(videos[0]) && videos[0].indexOf('failed') !== -1)
			if (isEmptyVideos)
				$scope.notFoundMsg = 'Фильмов по заданному фильтру не найдено'
			else
				$scope.videos = videos;

			waiterService.hide();
		});
	}]);

	hdserials.controller('LastViewedController', ['$scope', '$rootScope', '$state', 'VideosService', 'StorageService', function($scope, $rootScope, $state, videosService, storageService) {
		$scope.isShowed = false;

		var video = $scope.$parent.video,
			videoId = video.info.id,
			updateLastViewed;

		function updateLastViewed()
		{
			$scope.isShowed = false;

			var lastViewedInfo = storageService.getLastViwedInfo(videoId),
				file, lastViewedFile, percent, 
				i;

			if (!lastViewedInfo)
				return;

			$scope.isShowed = true;
			for (i = 0; i < video.files.length; i++)
			{
				file = video.files[i];
				if (file.id !== lastViewedInfo.fileId)
					continue;

				lastViewedFile = file;
				break;
			}

			$scope.isShowed = true;
		
			percent = Math.floor(lastViewedInfo.currentTime * 100 / lastViewedInfo.duration);
			$scope.lastViewedFile = {
				title: lastViewedFile.title,
				fileId: lastViewedFile.id,
				percent: percent,
				currentTime: lastViewedInfo.currentTime
			};
		}

		$scope.playVideoFile = function(fileId)	 {
			$state.go('video.play', { fileId: fileId, currentTime: $scope.lastViewedFile.currentTime });	
		}

		$rootScope.$on('updateLastViewedInfo', function(){
			updateLastViewed();
		});

		updateLastViewed();
	}]);

	hdserials.controller('SortBoxController', ['$scope', '$rootScope', 'StorageService', function($scope, $rootScope, storageService) {
		$scope.sortField = storageService.getSortField();
		$scope.sortDirection = storageService.getSortDirection();

		$scope.updateSortField = function() {
			storageService.setSortField($scope.sortField);
			$rootScope.$broadcast('sortSettingsChanged');
		};

		$scope.toggleSortDirection = function() {
			$scope.sortDirection = $scope.sortDirection === "asc" ? "desc" : "asc";
			storageService.setSortDirection($scope.sortDirection);
			$rootScope.$broadcast('sortSettingsChanged');
		};

		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 

		});
	}]);

	hdserials.controller('VisibleIfStateController', ['$scope', '$state', '$rootScope', function($scope, $state, $rootScope) {
		$scope.isVisible = false;
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 
			$scope.isVisible = toState.name === $scope.state;
		});
	}]);

	hdserials.controller('RatingBarController', ['$scope', '$state', '$rootScope', function($scope, $state, $rootScope) {
		
	}]);

	hdserials.controller('Html5PlayerController', ['$scope', function($scope) {
		var vstat;

		$scope.videoStatus =  {
			extended: false,
			isPlayed: false
		};

		vstat = $scope.videoStatus;

		$scope.$on('videoChangeState', function(event, args){
			$scope.videoStatus.isPlayed = args.isPlayed;
			$scope.$apply()
		})

		$scope.togglePlay = function() {	
			//vstat.isPlayed = !vstat.isPlayed;
			$scope.$emit('setVideoPlayState', { Play:!vstat.isPlayed })
			$scope.$apply()
		};

		$scope.toggleExtend = function() {	
			vstat.extended = !vstat.extended;
		};
	}]);

})();