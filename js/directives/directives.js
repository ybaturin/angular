'use strict';

/* Directives */

(function()
{
	var hdserials = angular.module('hdserials.directives', []);

	hdserials.directive('categories', ['$window', 'DOMService', function($window, DOMService) {
		return {
			restrict: 'E',
			replace: true,
			controller: 'CategoriesController',	
			templateUrl: "partials/categories.html",
			scope:
			{
				icons: "=hdIcons"
			},
			link: function($scope, elem, attr) {
				//TODO: нужно учитывать возможность ресайза окна

				var SUBCAT_MIN_HEIGHT = 100,
					doc = angular.element(document),
					domMenu = doc.find('.hd-nav-menu'),
					selectorSubCategorie = '.hd-sub-categorie, .hd-sub-categorie.ng-hide-remove.ng-hide-remove-active',
					subCatNodes = domMenu.find('.hd-sub-categorie');
					
				DOMService.addCSSSelector(selectorSubCategorie);

				$scope.updateSubCategoriesView = function(isVisible, catId)
				{
					if (!angular.isDefined(catId))
						return;

					var	domSelectedCat = domMenu.find('#categorie' + catId),
						domSubCat = domSelectedCat.find('.hd-sub-categorie'),
						domCatsWrap = domMenu.find('.hd-general-categorie-item > .hd-wrap'),
						catsWrapHeight = 0,
						domSubCatHeight, node, i;

					if (isVisible) {
						//поиск подходящей мак. высоты для ноды с сабкатегориями, при которой ноды других категорий будут влазить в экран
						for (i = 0; i < domCatsWrap.length; i++)
							catsWrapHeight+= angular.element(domCatsWrap[i]).outerHeight();

						domSubCatHeight = domMenu.height() - catsWrapHeight; 
						// if (domSubCatHeight > SUBCAT_MIN_HEIGHT)
						// domSubCat.css('max-height', domSubCatHeight + 'px');
						DOMService.setCSSSelectorProperty(selectorSubCategorie, 'maxHeight', domSubCatHeight + 'px');
					}
					else {
						// domSubCat.css('max-height', 0 + 'px');
						DOMService.setCSSSelectorProperty(selectorSubCategorie, 'maxHeight', 0 + 'px');
					}
				}

				$scope.$on('subCategoriesVisibleChanged', function(event, args)
				{
					$scope.updateSubCategoriesView(args.isVisible, args.catId);
				});


			}
		}
	}]);

	hdserials.directive('subcategories', function() {
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: "partials/subcategories.html",
			// controller: 'SubCategoriesController',	
			link: function($scope, elem, attr, ctrl) {
				//$scope.fillCategories();
			}
		}
	});

	hdserials.directive('hdWaiter', ['WaiterService', function(waiterService) {
		//дополнительные атрибуты для вейтера:
		//hd-show (type:boolean) - флаг, по которому показывается вейтер
		//hd-start-after (type:number) - число милисекунд, спустя которые показывать вейтер, если флаг hd-show всё ещё true
		return {
			restrict: 'AE',
			replace: true,
			template: '<div class="hd-waiter" ng-show="isShowed"><div class="hd-wrap"><i class="fa fa-spinner fa-spin"></i></div></div>',
			link: function($scope, elem, attr) {
				$scope.isShowed = false;

				$scope.$watch(function(){
					return waiterService.isShowed();
				}, function(newValue, oldValue) {
					$scope.isShowed = newValue;
				});	
			}
		}
	}]);

	// hdserials.directive('waiter', ['$timeout', function($timeout) {
	// 	//дополнительные атрибуты для вейтера:
	// 	//hd-show (type:boolean) - флаг, по которому показывается вейтер
	// 	//hd-start-after (type:number) - число милисекунд, спустя которые показывать вейтер, если флаг hd-show всё ещё true
	// 	return {
	// 		restrict: 'AE',
	// 		replace: true,
	// 		template: '<div class="waiter" ng-show="isShowed"><div class="waiterImg"></div></div>',
	// 		link: function($scope, elem, attr) {
	// 			var timeout = attr['hdStartAfter'],
	// 				watchParam = attr['hdShow'],
	// 				startWatch, checkShowWaiter;

	// 			if (!angular.isDefined(timeout))
	// 				timeout = 100;

	// 			$scope.isShowed = false;	

	// 			checkShowWaiter = function(){
	// 				$scope.isShowed = $scope[watchParam];
	// 			}

	// 			startWatch = function()
	// 			{
	// 				checkShowWaiter();
	// 				$scope.$watch(watchParam, function(){
	// 					//если отслеживаемый параметр изменился в состояние false,
	// 					//обрабатываем сразу
	// 					if (!$scope[watchParam])
	// 						checkShowWaiter();
	// 					else
	// 						$timeout(checkShowWaiter, timeout);
	// 				})
	// 			}

	// 			$timeout(startWatch, timeout);
	// 		}
	// 	}
	// }]);
	// <img src="img/loading.gif" class="hd-waiter-img"></img>

	function linkFnFadePosters ($scope, elem, attr) {
		var videoImg = elem.find('.hd-video-poster'),
			imgBg = elem.find('.hd-video-bg');

		videoImg.on('load', function()
		{
			imgBg.css('opacity', '0');
		});
	}
	hdserials.directive('videoGridItem', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			scope:
			{
				video: "=video"
			},
			controller: 'VideoGridItemController',	
			templateUrl: "partials/video_grid_item.html",
			link: linkFnFadePosters
		}
	}]);

	hdserials.directive('hdNewsGridItem', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			scope:
			{
				video: "=video"
			},
			controller: 'NewsGridItemController',	
			templateUrl: "partials/news_grid_item.html",
			link: linkFnFadePosters
		}
	}]);

	hdserials.directive('searchBox', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			controller: 'SearchBoxController',	
			templateUrl: "partials/search_box.html",
			link: function($scope, elem, attr) {
				
			}
		}
	}]);

	hdserials.directive('favoriteBox', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			scope:
			{
				video: "=video"
			},
			controller: 'FavoriteBoxController',	
			templateUrl: "partials/favorite_box.html",
			link: function($scope, elem, attr) {
				elem.on('click', function(event){
					event.stopPropagation();
				})
			}
		}
	}]);

	hdserials.directive('navigator', [function() {
		return {
			restrict: 'E',
			replace: true,
			transclude: true,
			controller: 'NavigatorController',	
			templateUrl: "partials/navigator.html",
			link: function($scope, elem, attr) {

			}
		}
	}]);

	hdserials.directive('html5Player', ['$window', function($window) {
		// обязательные атрибуты:
		// hd-src (type: string) - URL видео-файла
		return {
			restrict: 'AE',
			replace: true,
			scope: {
				src: "=hdSrc"
			},
			controller: 'Html5PlayerController',	
			templateUrl: "partials/html5player.html", 
			link: function($scope, elem, attr) {
				var video = elem.find('video'),
					videoNode = video[0],
					firstPlayEvent = true,
					firstCanPlay = true;

				$scope.$on('requestVideoTiming', function() {
					$scope.$emit('responseVideoTiming', { currentTime: domElem.currentTime, duration: domElem.duration });
				});

				video.on('play', function() {
					$scope.$emit('videoChangeState', { isPlayed: true });
				});

				video.on('pause', function() {
					$scope.$emit('videoChangeState', { isPlayed: false });
				});

				$scope.$on('setVideoPlayState', function(event, args) {
					if (args.Play)
						videoNode.play();
					else
						videoNode.pause();
				});

				// $scope.$on('setVideoExtend', function(event, args) {
				// 	if (args.Extend)
				// 		videoNode.play();
				// 	else
				// 		videoNode.pause();
				// });

				video.on('canplay', function() {
					if (!firstCanPlay)
						return;

					videoNode.currentTime = $scope.$parent.file.currentTime;
					firstCanPlay = false;
				});
			}
		}
	}]);

	hdserials.directive('flashPlayer', [function() {
		// обязательные атрибуты:
		// hd-src (type: string) - URL видео-файла
		return {
			restrict: 'AE',
			replace: true,
			scope: {
				src: "=hdSrc"
			},
			template: 
				'<object type="application/x-shockwave-flash" name="player" ng-attr-data="{{flashSrc}}">' + 
					'<param name="wmode" value="opaque">' + 
					'<param name="allowFullScreen" value="true">' + 
					'<param name="allowScriptAccess" value="always">' + 
					'<param name="flashvars" value="src={{src}}&amp;volumeInfoPattern=%D0%97%D0%B2%D1%83%D0%BA%20$$%25&amp;volumeInfoMutedString=Звук выключен&amp;emptyBufferTime=4&amp;expandedBufferTime=30&amp;autoPlay=true&amp;bgcolor=#000000&amp;javascriptCallbackFunction=playerCallback&amp;controlBarAutoHideTimeout=8&amp;autoRewind=false&amp;src_http://kutu.ru/osmf/plugins/subtitles=%7B%22subtitles%22%3A%5B%5D%2C%22config%22%3A%7B%22fontSize%22%3A0.035%2C%22minFontSize%22%3A14%2C%22maxFontSize%22%3A36%2C%22textColor%22%3A14671839%2C%22bgColor%22%3A1052688%2C%22bgAlpha%22%3A0.8%7D%7D">' + 
				'</object>',
			link: function($scope, element, attr) {
				$scope.flashSrc = 'http://moonwalk.cc/player_v01.swf?001';
			}
		}
	}]);

	// base code taked from https://github.com/EricWVGG/AngularSlideables
	hdserials.directive('hdCollapsible', function () {
		return {
			restrict:'C',
			compile: function (element, attr) {
				// wrap tag
				var contents;

				contents = element.html();
				element.html('<div class="hd-collapsible-content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

				return function postLink(scope, element, attrs) {
					if (angular.isDefined(attrs.hdCollapsibleEnabled) && attrs.hdCollapsibleEnabled === 'false') {
						return;
					}

					// default properties
					attrs.duration = (!attrs.duration) ? '0.5s' : attrs.duration;
					attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
					element.css({
						'overflow': 'hidden',
						'height': '0px',
						'transitionProperty': 'height',
						'transitionDuration': attrs.duration,
						'transitionTimingFunction': attrs.easing
					});
				};
			}
		};
	});

	hdserials.directive('hdCollapseToggler', function() {
		var collapsibles = {};
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var target, 
					content;

				target = element[0].parentElement.querySelector(attrs.hdCollapseToggler)
				content = target.querySelector('.hd-collapsible-content');

				collapsibles[attrs.hdCollapseGroup] = collapsibles[attrs.hdCollapseGroup] || [];
				collapsibles[attrs.hdCollapseGroup].push({
					node: target,
					attrs: attrs
				});
				attrs.expanded = false;
				
				element.bind('click', function() {
					var targets, i;
					
					if (!attrs.expanded) {
						targets = collapsibles[attrs.hdCollapseGroup];
						// collapse other targets from root
						for (i = 0; i < targets.length; i++) {
							if (targets[i].node != target) {
								targets[i].node.style.height = '0px';
								targets[i].attrs.expanded = false;
							}
						}

						target.style.height = content.clientHeight + 'px';
					} 
					else {
						target.style.height = '0px';
					}

					attrs.expanded = !attrs.expanded;
				});
			}
		}
	});

	hdserials.directive('hdMoveToBody', function () {
		return {
			restrict: 'A',
			compile: function (element, attr) {
				return function postLink(scope, element, attrs) {
					document.body.appendChild(element[0]);
				};
			}
		};
	});

	hdserials.directive('hdGlitchText', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			scope:
			{
				text: "@"
			},
			// controller: 'FavoriteBoxController',	
			templateUrl: "partials/glitch_text.html",
			link: function($scope, elem, attr) {
				elem.on('click', function(event){
					event.stopPropagation();
				})
			}
		}
	}]);

	hdserials.directive('hdFilter', [function() {
		return {
			restrict: 'E',
			replace: true,
			controller: 'FilterController',	
			templateUrl: "partials/filter.html",
			link: function($scope, elem, attr) {

			}
		}
	}]);

	hdserials.directive('hdLastViewed', ['$window', function($window) {
		return {
			restrict: 'E',
			replace: true,
			scope:
			{
				videoId: "@"
			},
			controller: 'LastViewedController',	
			templateUrl: "partials/last_viewed.html",
			link: function($scope, elem, attr) {
				
			}
		}
	}]);
	hdserials.directive('hdSortBox', [function() {
		return {
			restrict: 'E',
			replace: true,
			controller: 'SortBoxController',	
			templateUrl: "partials/sortbox.html",
			link: function($scope, elem, attr) {
				
			}
		}
	}]);

	hdserials.directive('hdContent', ['$rootScope', function($rootScope) {
		return {
			link: function($scope, elem, attr) {
				$rootScope.$on('sortSettingsChanged', function() {
					elem[0].scrollTop = 0;
				});
			}
		}
	}]);

	hdserials.directive('hdVisibleIfState', [function() {
		return {
			restrict: 'AE',
			scope:
			{
				state: "@"
			},
			replace: true,
			template: '<div ng-show="isVisible" ng-transclude />',
			transclude: true,
			controller: 'VisibleIfStateController',	
			link: function($scope, elem, attr) {
			}
		}
	}]);

	hdserials.directive('hdRatingBar', [function() {
		return {
			scope:
			{
				rating: "@",
				maxRating: "@"
			},
			restrict: 'E',
			replace: true,
			templateUrl: "partials/rating_bar.html",
			controller: 'RatingBarController',	
			link: function($scope, elem, attr) {
				if (!angular.isDefined($scope.rating))
					return;

				var maxRating = $scope.maxRating || 10,
					width = $scope.rating * 100 / maxRating, 
					oneThird = maxRating / 3,
					elemClass = 'hd-rating-bar',
					filledNode = elem.find('.' + elemClass + '__filled'),
					numberNode = elem.find('.' + elemClass + '__number'),
					ticksParentNode = elem.find('.' + elemClass + '__ticks'),
					tickNodes = '',
					tickInterval, i;

				filledNode.css('width', width + '%');
				numberNode[0].innerHTML = $scope.rating;

				if ($scope.rating < oneThird)
				{
					elem.addClass(elemClass + '--low-rating');
				}
				else if ($scope.rating >= oneThird && $scope.rating < oneThird * 2)
				{
					elem.addClass(elemClass + '--middle-rating');
				}
				else
				{
					elem.addClass(elemClass + '--high-rating');
				}

				for (i = 1; i <= maxRating; i++)
				{
					tickInterval = 100 * i / maxRating;
					tickNodes += '<div class="' + elemClass + '__ticks__node" style="left: ' + tickInterval + '%"></div>'
				}
				ticksParentNode.append(tickNodes);
			}
		}
	}]);
})();