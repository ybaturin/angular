(function()
{
	var hdserials = angular.module('hdserials.directives_utils', []);
	hdserials.directive('ngEnter', function () {
		return function ($scope, elem, attr) {
			elem.bind("keydown keypress", function (event) {
				if(event.which === 13) {
					$scope.$apply(function (){
						$scope.$eval(attr.ngEnter);
					});
	 
					event.preventDefault();
				}
			});
		};
	});

	hdserials.directive('hdUiSref', function () {
		return {
			restrict: 'A',
			replace: true,
			controller: ['$scope', '$state', '$stateParams', 'StorageService', function($scope, $state, $stateParams, storageService) {
				$scope.go2State = function(state, params) {
					$state.go(state, params);
				};
			}],

			link: function($scope, elem, attr) {
				var state = attr.hdUiSref,
					params = $scope.$eval(attr.hdUiParams);

				elem.on("click", function (event) {
					$scope.go2State(state, params);
					event.preventDefault()
				});
			}
		}
	});

	hdserials.directive('autoresize', ['$window', '$timeout', function($window, $timeout) {
		return {
			restrict: 'A',
			replace: false,
			// controller: 'SubCategoriesController',	
			link: function($scope, elem, attr) {
				var CALL_USERFUNC_TIMEOUT = 200,
					w = angular.element($window),
					resizeType = attr.autoresize || 'full',
					rect, timer;
				
				//elem.css('overflow', 'auto');
				function getWidth(rect)
				{
					return w.prop('innerWidth') - rect.left;
				}

				function getHeight(rect)
				{
					return w.prop('innerHeight') - rect.top;
				}

				function callUserFunc()
				{
					$scope.$eval(attr.hdAutoresizeFunc);
				};

				function callUserFuncTimeout()
				{
					if (!attr.hdAutoresizeFunc)
						return;

					$timeout.cancel(timer);
					timer = $timeout(callUserFunc, CALL_USERFUNC_TIMEOUT)
				}

				function updateSize()
				{
					rect = elem[0].getBoundingClientRect();
					switch(resizeType)
					{
						case 'full':
							elem.css('width', getWidth(rect) + 'px');
							elem.css('height', getHeight(rect) + 'px');
							break;

						case 'width':
							elem.css('width', getWidth(rect) + 'px');
							break;

						case 'height':
							elem.css('height', getHeight(rect) + 'px');
							break;
					}

					callUserFuncTimeout();
				}
			
				w.on('resize', updateSize);
				updateSize();
			}
		}
	}]);

	hdserials.directive('hdRepeatFinished', ['$timeout', function ($timeout) {
		return function ($scope, element, attr) {
			if (!$scope.$last)
				return;

			//только на таймер можно весить изменение модели
			$timeout(function (){
				$scope.$eval(attr.hdRepeatFinished);
			});
		};
	}]);

	hdserials.directive('hdAnimateMarkSelector', ['$timeout', function ($timeout) {
		return function ($scope, element, attr) {
			var markElementSelector = attr.hdAnimateMarkSelector,
				markElement = angular.element(document).find(markElementSelector);
			if (!markElement)
				return;

			element.on("$animate:before", function()
			{
				markElement.addClass('hd-animate');
			});
			element.on("$animate:close", function()
			{
				markElement.removeClass('hd-animate');
			});
		};
	}]);

	hdserials.directive('hdBeforeUnload', ['$window', function ($window) {
		return function ($scope, element, attr) {
			var w = angular.element($window);

			w.on("beforeunload", function()
			{
				$scope.$eval(attr.hdBeforeUnload);
			});
		};
	}]);
})();