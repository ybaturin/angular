'use strict';

(function() {
	// Declare app level module which depends on filters, and services
	var hdserials = angular.module('hdserials', [
		'ui.router',
		'ngAnimate',
		'infinite-scroll',
		'hdserials.filters',
		'hdserials.services',
		'hdserials.controllers',
		'hdserials.directives',
		'hdserials.directives_utils'
	]);

	hdserials.config(['$compileProvider', function ($compileProvider) {
		// https://code.angularjs.org/1.3.0-rc.1/docs/guide/production
		//$compileProvider.debugInfoEnabled(false);
	}]);

	hdserials.config(['$animateProvider', function($animateProvider){
		// restrict animation to elements with the bi-animate css class with a regexp.
		// note: "bi-*" is our css namespace at @Bringr.
		$animateProvider.classNameFilter(/^((?!hd-dont-animate).)*$/);
	}]);

	hdserials.config(function($stateProvider, $urlRouterProvider) {
		// For any unmatched url, redirect to /categories
//		$urlRouterProvider
//			.when('/user/:id', '/contacts/:id')
//			.otherwise('/');
		$urlRouterProvider.otherwise("/category/");
		
		// Now set up the states
		$stateProvider.state('category', {
			url: "/category/{id:[0-9]*}",
			templateUrl: "partials/videos_list.html",
			controller: 'VideosListController'
		});

		$stateProvider.state('video', {
			url: "/video/{id:[0-9]+}",
			templateUrl: "partials/video_details.html",
			controller: 'VideoDetailsController',
			resolve: {
				videoId: ['$stateParams', function($stateParams) {
					return $stateParams.id;
				}],
				videoDetails: ['$stateParams', 'WaiterService', 'VideosService', function($stateParams, waiterService, videosService) {
					var deferer;
					waiterService.show();
					deferer = videosService.getVideoDetail($stateParams.id);
					deferer.then(function() {
						waiterService.hide();
					});
					return deferer;
				}]
			}
		});

		$stateProvider.state('video.play', {
			url: "/play/{fileId:[0-9]+}?currentTime",
			templateUrl: "partials/video_player.html",
			controller: 'VideoPlayerController'
		});

		$stateProvider.state('search', {
			url: "/search/?name",
			templateUrl: "partials/search_result.html",
			controller: 'SearchStateController'
		});

		$stateProvider.state('favorites', {
			url: "/favorites",
			templateUrl: "partials/favorites.html",
			controller: 'FavoritesStateController'
		});

		$stateProvider.state('news', {
			url: "/news",
			templateUrl: "partials/news.html",
			controller: 'NewsStateController'
		});

		$stateProvider.state('filter', {
			url: "/filter/?year_from&year_to&country&genre",
			templateUrl: "partials/filter_result.html",
			controller: 'FilterStateController'
		});
	});
})();