'use strict';

/* Services */

(function() {
	// Declare app level module which depends on filters, and services
	var hdserials = angular.module('hdserials.services', []);
	var CACHE_ON = true;

	hdserials.factory('VideosService', ['$http', '$rootScope', '$q', 'StorageService', 'HTTPCacheService', function($http, $rootScope, $q, storageService, HTTPCacheService) { 
		var JSONP_URL = "http://hdserials.galanov.net/backend/model.php?skip_ua=yep&callback=JSON_CALLBACK&",
			VIDEOS_LIMIT = 20,
			service = {},
			subCategories = {},
			videosList = null, //TODO сделать кэширование списка фильмов и конкретного фильма
			video = null,
			generalCategories = null,
			filterData = null;
	 
		service.setSelectedCategorie = function(value) {
			this.selectedCategorie = value;
		}
	 
		service.getSelectedCategorie = function() {
			return this.selectedCategorie;
		}

		service.getGeneralCategories = function() {
			var deferred = $q.defer(),
				params = 'id=common-categories';
			
			if (generalCategories)
				deferred.resolve(generalCategories);
			else
			{
				HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
					generalCategories = response;
					deferred.resolve(generalCategories);
				});
			}
			
			return deferred.promise;
		}

		service.getSubCategories = function(parentId) {
			var deferred = $q.defer(),
				params = 'id=sub-categories&parent=' + parentId;
			
			if (subCategories[parentId])
				deferred.resolve(subCategories[parentId]);
			else
			{		
				HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
					subCategories[parentId] = response;
					deferred.resolve(subCategories[parentId]);
				});
			}
			
			return deferred.promise;
		}
		
		service.getVideosList = function(catId, startPosition, limit) {
			limit = limit || VIDEOS_LIMIT;

			var deferred = $q.defer(),
				sortField = storageService.getSortField(),
				sortOrder = storageService.getSortDirection() === "desc" ? 1 : 0,
				params = 'id=filter-videos&category=' + catId + '&detailed=0&fresh=0&start=' + startPosition + '&order_by=' + sortField + '&order_desc=' + sortOrder + '&limit=' + limit;
			
			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				videosList = response;
				deferred.resolve(videosList);
			});

			return deferred.promise;
		}
		
		service.getVideoDetail = function(videoId) {
			var deferred = $q.defer(),
				params = 'id=video&video=' + videoId;
			
			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				var video = response.data,
					files = video.files, 
					seasons = {}, 
					seasonNum, episodeNum, 
					countries = [],
					translationId, translation, hashKey, i;

				for (i = 0; i < files.length; i++) {
					seasonNum = parseInt(files[i].season);
					episodeNum = parseInt(files[i].episode);
					translationId = parseInt(files[i].season_translation_id || "0");
					video.hasSeasons = episodeNum > 0;
					hashKey = seasonNum + '|' + translationId;

					if (!seasons[hashKey]) {
						translation = files[i].season_translation ? ' (' + files[i].season_translation + ')' : '';
						seasons[hashKey] = {
							episodes: [],
							title: 'Сезон ' + seasonNum + translation
						};
					}

					files[i].viewed = storageService.isViewedFile(files[i].id);
					seasons[hashKey].episodes.push(files[i]);
				}
				video.seasons = seasons;

				for (i = 0; i < video.countries.length; i++) {
					countries.push(video.countries[i].title_ru);
				}
				video.yearAndCountry = video.info.year ? video.info.year : '';
				video.yearAndCountry += countries.length > 0 ? ', ' + countries.join(', ') : '';

				deferred.resolve(video);
			});

			
			return deferred.promise;
		};

		service.getVideoFileInfo = function(fileId) {
			var deferred = $q.defer(),
				params = 'id=video-file&file=' + fileId;
			
			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				var fileInfo = response.data;
				deferred.resolve(fileInfo);
			});
			
			return deferred.promise;
		};
	
		service.searchVideos = function(args) {
			var deferred = $q.defer(),
				name = encodeURI(args.name),
				params = 'id=filter-videos&search=' + name;
			
			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				deferred.resolve(response.data);
			});
			
			return deferred.promise;
		};

		service.getNews = function() {
			var deferred = $q.defer(),
				params = 'id=news';
			
			//TODO пока пусть будет всегда запрос актуальных данных, всё-таки новости
			$http.jsonp(JSONP_URL + params).success(function(response) {
				deferred.resolve(response.data);
			});
			
			return deferred.promise;
		};

		service.getFilterData = function() {
			var deferred = $q.defer(),
				params = 'id=filter-data';
			
			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				deferred.resolve(response.data);
			});
			
			return deferred.promise;
		};

		service.filterVideos = function(args) {
			var deferred = $q.defer(),
				name = encodeURI(args.name),
				params = 'id=filter-videos',
				argProp, value;
			
			for (argProp in args)
			{
				value = args[argProp];
				if (!value)
					continue;

				params += '&' + argProp + '=' + value;
			}

			HTTPCacheService.jsonp(JSONP_URL + params).then(function(response) {
				deferred.resolve(response.data);
			});
			
			return deferred.promise;
		};

		return service;
	}]);

	hdserials.factory('StorageService', ['$http', '$rootScope', '$q', '$window', function($http, $rootScope, $q, $window) {
		var STRUCTURE_ID = 'hdserials_storageService',
			structure = localStorage.getItem(STRUCTURE_ID),
			w = angular.element($window),
			service = {},
			favorites, viewedFiles, httpCache, lastViewed;

		w.on('unload', function(){
			localStorage.setItem(STRUCTURE_ID, JSON.stringify(structure));
		})

		//TODO сделать через extend объекта для большей читабельности
		structure = structure ? JSON.parse(structure) : {};
		favorites = structure.FavoriteVideos = structure.FavoriteVideos || {};
		viewedFiles = structure.ViewedFiles = structure.ViewedFiles || [];
		httpCache = structure.HttpCache = structure.HttpCache || {};

		//структура:
		// {
		// 	videoId: {
		// 		fileId: #id последнего просмотренного файла
		// 		currentTime: #позиция видео-скрола
		// 		duration: #длительность видео-файла	
		// 	}
		// }		
		lastViewed = structure.LastViewed = structure.LastViewed || {};
		structure.SortSettings = structure.SortSettings || {};

		service.toggleFavoriteVideo = function(video) {
			if (service.isFavoriteVideo(video))
				service.removeFavoriteVideo(video);
			else
				service.addFavoriteVideo(video);
		}

		service.isFavoriteVideo = function(video) {
			return !!favorites[video.id];
		}

		service.removeFavoriteVideo = function(video) {
			delete favorites[video.id];
		}

		service.addFavoriteVideo = function(video) {
			favorites[video.id] = video;
		}

		service.getFavoriteVideos = function() {
			var result = [],
				video;

			for (video in favorites)
				result.push(favorites[video])

			return result;
		}

		// viewes files states
		service.toggleViewedFile = function(fileId) {
			if (service.isViewedFile(fileId)) {
				service.removeViewedFile(fileId);
			}
			else {
				service.addViewedFile(fileId);
			}
		}

		service.isViewedFile = function(fileId) {
			return viewedFiles.indexOf(fileId) > -1;
		}

		service.removeViewedFile = function(fileId) {
			var index = viewedFiles.indexOf(fileId);
			if (index === -1) {
				return;
			}

			viewedFiles.splice(index, 1);
		}

		service.addViewedFile = function(fileId) {
			viewedFiles.push(fileId);
		}

		service.updateLastViewedInfo = function(videoId, fileId, timingInfo) {
			lastViewed[videoId] = {
				fileId: fileId,
				currentTime: timingInfo.currentTime,
				duration: timingInfo.duration
			};

			$rootScope.$broadcast('updateLastViewedInfo');
		}

		service.getLastViwedInfo = function(videoId) {
			return lastViewed[videoId];
		}


		service.getSortField = function() {
			return this.get('SortSettings', 'Field', 'title_ru')
		}

		service.getSortDirection = function() {
			return this.get('SortSettings', 'Direction', 'desc')
		}

		service.setSortField = function(value) {
			this.update('SortSettings', 'Field', value)
		}

		service.setSortDirection = function(value) {
			this.update('SortSettings', 'Direction', value)
		}

		//#region abstract CRUD

		service.get = function(propertyName, id, defaultValue) {
			var property = structure[propertyName];

			if (!angular.isDefined(id))
				return property;

			return angular.isDefined(property[id]) ? property[id] : defaultValue;
		}

		service.add = function(propertyName, element, id) {
			var property = structure[propertyName];

			if (angular.isArray(property))
				property.push(element);
			else 
			{
				console.assert(angular.isDefined(id));
				property[id] = element;
			}
		}

		service.update = function(propertyName, id, element) {
			var property = structure[propertyName];
			property[id] = element;
		}

		service.remove = function(propertyName, elementOrId) {
			var property = structure[propertyName],
				index;

			if (angular.isArray(property))
			{
				index = property.indexOf(elementOrId);
				if (index !== -1)
					property.splice(index, 1);
			}
			else 
				delete property[elementOrId];
		}
		//#endregion abstract CRUD

		return service;
	}]);

	hdserials.factory('HTTPCacheService', ['$http', '$q', 'StorageService', function($http, $q, storageService) {
 		var DEFAULT_TIMEOUT_MIN = 30,
			MAX_COUNT_ELEMENTS = 100,
			MS_IN_MIN = 1000 * 60,
			REMOVE_LAST_COUNT = Math.floor(MAX_COUNT_ELEMENTS / 10) || 1,
			cache = storageService.get('HttpCache'),
			service = {},
			count, addToCache, getFromCache, url, removeFromCache,
			isElementOutdated, cleaningCache, sortCacheArrayFn;

		isElementOutdated = function(element, currentTime, timeout)
		{
			timeout = timeout || element.timeout;
			return ((currentTime - element.time) / MS_IN_MIN) > timeout;
		};

		removeFromCache = function(url)
		{
			if (!cache[url])
				return;

			delete cache[url];
			count--;
		};

		addToCache = function(url, response, timeout)
		{
			var currentTime = new Date();
			cache[url] = {
				url: url,
				time: currentTime,
				timeout: timeout || DEFAULT_TIMEOUT_MIN,
				response: response
			}

			count++;
			cleaningCache();
		};

		sortCacheArrayFn = function(e1, e2)
		{
			if (e1.time < e2.time)
				return 1;
			if (e1.time > e2.time)
				return -1;
			return 0;
		}

		cleaningCache = function(force)
		{
			if (!force && count <= MAX_COUNT_ELEMENTS)
				return;

			var currentTime = new Date(),
				max_elements = MAX_COUNT_ELEMENTS - REMOVE_LAST_COUNT,
				elementsArray = [],
				url, element, i;

			for (url in cache)
			{
				element = cache[url];
				if (isElementOutdated(element, currentTime))
				{
					removeFromCache(url);
					continue;
				}

				elementsArray.push(element);
			}

			if (elementsArray.length > max_elements)
			{
				elementsArray.sort(sortCacheArrayFn);
				for (i = max_elements; i < elementsArray.length; i++)
					removeFromCache(elementsArray[i].url);
			}

		};

		getFromCache = function(url, timeout)
		{
			var result = cache[url],
				currentTime;

			if (!result)
				return null;

			currentTime = new Date();
			if (isElementOutdated(result, currentTime, timeout))
			{
				removeFromCache(url);
				return null;
			}
			else
			{
 				result.time = currentTime;
 				return result.response;
			}
		};

		service.jsonp = function(url, timeout)
		{
			var deferred = $q.defer(),
				result = getFromCache(url, timeout);

			if (result && CACHE_ON)
			{
				console.log('cache hit: ', url);
				deferred.resolve(result)
			}
			else
			{
				$http.jsonp(url).success(function(response) {
					if (CACHE_ON)
					{
						// console.warn('cache miss:', url); //DEBUG
						addToCache(url, response, timeout);
					}

					deferred.resolve(response);
				});
			}

			return deferred.promise;
		};

		for(url in cache)
			cache[url].time = new Date(cache[url].time);

		count = Object.keys(cache).length;
		// window.cache = cache; //DEBUG
		cleaningCache(true);

		return service;
	}]);

	hdserials.factory('WaiterService', ['$timeout', function($timeout) {
		var SHOW_TIMEOUT = 100,
			showCallCount = 0,
			service = {},	
			isStartShowed = false,
			isShowed = false,
			timeoutPromise;

		isShowed = false;	

		service.isShowed = function(){
			return isShowed;
		}

		service.show = function()
		{
			showCallCount++;
			if (isStartShowed || isShowed)
				return;

			timeoutPromise = $timeout(function(){
				isShowed = true;
				isStartShowed = false;
			}, SHOW_TIMEOUT);

			isStartShowed = true;
		}

		service.hide = function()
		{
			showCallCount--;
			if (showCallCount === 0)
			{
				isShowed = false;
				isStartShowed = false;
				if (timeoutPromise)
					$timeout.cancel(timeoutPromise);
			}
		}

		return service;
	}]);

	hdserials.factory('DOMService', [function() {
		var service = {},
			rulesCache = {},
			getStyleSheet, rules,
			styleSheetInstance;
		
		getStyleSheet = function()
		{
			var head, style;
			if (!styleSheetInstance)
			{
				head = document.head || document.getElementsByTagName('head')[0];
				style = document.createElement('style');
				style.type = 'text/css';
				if (style.styleSheet){
					style.styleSheet.cssText = '';
				} 
				else {
					style.appendChild(document.createTextNode(''));
				}

				head.appendChild(style);
				styleSheetInstance = document.styleSheets[document.styleSheets.length - 1];
				rules = styleSheetInstance.cssRules ? styleSheetInstance.cssRules : styleSheetInstance.rules;
			}

			return styleSheetInstance;
		};

		service.addCSSSelector = function(selector)
		{
			var	styleSheet = getStyleSheet();

			if(styleSheet.addRule){
				styleSheet.addRule(selector);
			}
			else if(styleSheet.insertRule){
				styleSheet.insertRule(selector + '{}', styleSheet.cssRules.length);
			}

			rulesCache[selector] = rules[rules.length - 1];
		}

		service.setCSSSelectorProperty = function(selector, property, value)
		{
			var rule = rulesCache[selector];

			if (!rule)
				service.addCSSSelector(selector);

			rule.style[property] = value;
		}

		return service;
	}]);

	hdserials.factory('BrowserDetector', [function() {	
		var service = {};

		service.isTouch = function () {
			return !!window.ontouchstart;
		},

		service.isPortrait = function () {
			return window.innerHeight > window.innerWidth;
		},

		service.isSmartTV = function()
		{
			return navigator.userAgent.search(/TV/i) >= 0;
		};

		service.isAndroid = function() {
			return navigator.userAgent.match(/Android/i) ? true : false;
		};

		service.isBlackBerry = function() {
			return navigator.userAgent.match(/BlackBerry/i) ? true : false;
		};

		service.isIOS = function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
		};

		service.isWindowsPhone = function() {
			return navigator.userAgent.match(/IEMobile/i) ? true : false;
		};

		service.isMobile = function() {
			return (
				service.isAndroid() || 
				service.isBlackBerry() || 
				service.isIOS() || 
				service.isWindowsPhone()
			);
		};

		return service;
	}]);
})();