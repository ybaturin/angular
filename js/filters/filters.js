'use strict';

/* Filters */

(function() {
	var hdserials = angular.module('hdserials.filters', []);
	hdserials.filter('trusted', ['$sce', function ($sce) {
		// фильтр обеспечивает проверку урла через Strict Contextual Escaping (SCE) 
		// https://docs.angularjs.org/error/$sce/insecurl
		return function(url) {
			return $sce.trustAsResourceUrl(url);
		};
	}]);

	hdserials.filter('multilangTitle', [function () {
		// вывод заголовка объекта содержащего русский и возможно английский варианты
		return function(object) {
			if (!object)
				return object;
			
			var result = object.title_ru || object.video_title_ru,
				title_en = object.title_en || object.video_title_en,
				season = object.season || object.video_season;
				
			if (title_en) {
				result += ' / ' + title_en;
			}
			if (season) {
				result += ' (' + season + ')';
			}
			return result;
		};
	}]);
})();